const apierrors = {
	"error_1" : {
		"type": "/db/db-connexion-error",
		"status" : 503,
		"message" : "Erreur de connection à la base de données",
		"details" : "Une erreur est survenue lors de la connexion à la base de données"
	},
	"error_2" : {
		"type": "/db/data-not-found",
		"status" : 404,
		"message" : "Données introuvables dans la Base de données",
		"details" : "Les données demandées sont introuvables dans la Base de données."
	},
	"error_3" : {
		"type": "/errors/geojson-parsing-error",
		"status" : 500,
		"message" : "Erreur de conversion de données en geojson",
		"details" : "Une erreur est survenue lors de la conversion des données en geojson."
	},
	"error_4" : {
		"type": "/errors/search",
		"status" : 400,
		"message" : "Une erreur inattendue est survenue",
		"details" : "Une erreur inattendue est survenue lors de la recherche. Veillez contacter l'administrateur si le problème persiste."
	},
	"default" : {
		"type": "/errors/error",
		"status" : 400,
		"message" : "Une erreur inattendue est survenue",
		"details" : "Une erreur inattendue est survenue. Veillez contacter l'administrateur si le problème persiste."
	}
};

module.exports = apierrors;