var dbgeo = require('dbgeo');
var geojsonhint = require('@mapbox/geojsonhint')
var pool = require('./credentials');
var apierrors = require('../models/apierrors')



var getGeoJsonParcels = async (callback) => {
	/* Select the qip and geometry as a GeoJSON object in the field 'geometry' */
    pool.query('SELECT qip, parcelle as label, quartier, quartierc, numlot, nomprenom_rec, nom_proprio_maj, source_maj, ST_AsGeoJSON(geom) AS geometry from parcelles', function(error, result) {
    	if (error) {
    		// Database connexion error
		    console.error("Database connexion/query error : ", error.stack);
		    callback(apierrors['error_1']);
		}
		else if (!result) {
			console.error("Database query invalid response");
		    callback(null, {});
		}
		else {
	    	dbgeo.parse(result.rows, {
				geometryType: 'geojson',
				geometryColumn: 'geometry'
	      	}, function(error, result) {
				if (error) {
					// Geojson parsing error
		    		console.error("Geojson parsing error : ", error.stack);
					callback(apierrors['error_3']);
				} 
				else {
					if(typeof callback === 'function') {
						result.crs = {
							"type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::3857" }
						};
						callback(null, result);
					}
				}
			});
    	}
    });
};



var getGeoJsonRoutes = async (callback) => {
	/* Select routes as a GeoJSON object in the field 'geometry' */
    pool.query('SELECT ST_AsGeoJSON(geom) AS geometry from routes', function(error, result) {
    	if (error) {
    		// Database connexion error
		    console.error("Database connexion/query error : ", error.stack);
		    callback(apierrors['error_1']);
		}
		else if (!result) {
			console.error("Database query invalid response");
		    callback(null, {});
		}
		else {
	    	dbgeo.parse(result.rows, {
				geometryType: 'geojson',
				geometryColumn: 'geometry'
	      	}, function(error, result) {
				if (error) {
					// Geojson parsing error
		    		console.error("Geojson parsing error : ", error.stack);
					callback(apierrors['error_3']);
				}
				else {
					if(typeof callback === 'function') {
						result.crs = {
							"type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::3857" }
						};
						callback(null, result);
					}
				}
			});
    	}
    });
};

var getGeoJsonIlots = async (callback) => {
	/* Select ilots as a GeoJSON object in the field 'geometry' */
    pool.query('SELECT numilot as label, ST_AsGeoJSON(geom) AS geometry from ilots_reserves', function(error, result) {
    	if (error) {
    		// Database connexion error
		    console.error("Database connexion/query error : ", error.stack);
		    callback(apierrors['error_1']);
		}
		else if (!result) {
			console.error("Database query invalid response");
		    callback(null, {});
		}
		else {
	    	dbgeo.parse(result.rows, {
				geometryType: 'geojson',
				geometryColumn: 'geometry'
	      	}, function(error, result) {
				if (error) {
					// Geojson parsing error
		    		console.error("Geojson parsing error : ", error.stack);
					callback(apierrors['error_3']);
				}
				else {
					if(typeof callback === 'function') {
						result.crs = {
							"type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::3857" }
						};
						callback(null, result);
					}
				}
			});
    	}
    });
};

var getGeoJsonQuartiers = async (callback) => {
	/* Select quartiers as a GeoJSON object in the field 'geometry' */
    pool.query('SELECT quartier as label, ST_AsGeoJSON(geom) AS geometry from quartiers', function(error, result) {
    	if (error) {
    		// Database connexion error
		    console.error("Database connexion/query error : ", error.stack);
		    callback(apierrors['error_1']);
		}
		else if (!result) {
			console.error("Database query invalid response");
		    callback(null, {});
		}
		else {
	    	dbgeo.parse(result.rows, {
				geometryType: 'geojson',
				geometryColumn: 'geometry'
	      	}, function(error, result) {
				if (error) {
					// Geojson parsing error
		    		console.error("Geojson parsing error : ", error.stack);
					callback(apierrors['error_3']);
				}
				else {
					if(typeof callback === 'function') {
						result.crs = {
							"type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::3857" }
						};
						callback(null, result);
					}
				}
			});
    	}
    });
};

var getGeoJsonArrondissements = async (callback) => {
	/* Select ilots as a GeoJSON object in the field 'geometry' */
    pool.query('SELECT arrondiss as label, ST_AsGeoJSON(geom) AS geometry from arrondissements', function(error, result) {
    	if (error) {
    		// Database connexion error
		    console.error("Database connexion/query error : ", error.stack);
		    callback(apierrors['error_1']);
		}
		else if (!result) {
			console.error("Database query invalid response");
		    callback(null, {});
		}
		else {
	    	dbgeo.parse(result.rows, {
				geometryType: 'geojson',
				geometryColumn: 'geometry'
	      	}, function(error, result) {
				if (error) {
					// Geojson parsing error
		    		console.error("Geojson parsing error : ", error.stack);
					callback(apierrors['error_3']);
				}
				else {
					if(typeof callback === 'function') {
						result.crs = {
							"type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::3857" }
						};
						callback(null, result);
					}
				}
			});
    	}
    });
};

var getGeoJsonInfrastructures = async (callback) => {
	/* Select ilots as a GeoJSON object in the field 'geometry' */
    pool.query('SELECT ST_AsGeoJSON(geom) AS geometry from infrastructures_socio_communautaires', function(error, result) {
    	if (error) {
    		// Database connexion error
		    console.error("Database connexion/query error : ", error.stack);
		    callback(apierrors['error_1']);
		}
		else if (!result) {
			console.error("Database query invalid response");
		    callback(null, {});
		}
		else {
	    	dbgeo.parse(result.rows, {
				geometryType: 'geojson',
				geometryColumn: 'geometry'
	      	}, function(error, result) {
				if (error) {
					// Geojson parsing error
		    		console.error("Geojson parsing error : ", error.stack);
					callback(apierrors['error_3']);
				}
				else {
					if(typeof callback === 'function') {
						result.crs = {
							"type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::3857" }
						};
						callback(null, result);
					}
				}
			});
    	}
    });
};

/*
Search request
*/
var getGeoJsonFromQuery = async (text, values, callback) => {
	/* Select the qip and geometry as a GeoJSON object in the field 'geometry' */
    pool.query(text, values, function(error, result) {
    	if (error) {
    		// Database connexion error
		    console.error("Database connexion/query error : ", error.stack);
		    callback(apierrors['error_1']);
		}
		else if (!result) {
			console.error("Database query invalid response");
		    callback(null, {});
		}
		else {
	    	dbgeo.parse(result.rows, {
				geometryType: 'geojson',
				geometryColumn: 'geometry'
	      	}, function(error, result) {
				if (error) {
					// Geojson parsing error
		    		console.error("Geojson parsing error : ", error.stack);
					callback(apierrors['error_3']);
				} 
				else {
					if(typeof callback === 'function') {
						result.crs = {
							"type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::3857" }
						};
						callback(null, result);
					}
				}
			});
    	}
    });
};


module.exports = {
	getGeoJsonParcels,
	getGeoJsonRoutes,
	getGeoJsonIlots,
	getGeoJsonQuartiers,
	getGeoJsonArrondissements,
	getGeoJsonInfrastructures,
	getGeoJsonFromQuery
}