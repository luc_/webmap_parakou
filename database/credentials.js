const { Pool } = require('pg')

// DB config
var config = {
  user: 'postgres',
  host: '192.168.0.2',
  port: 5432,
  database: "dbparakou",
  password: 'mypassword',
  statement_timeout : 10000, // number of milliseconds before a statement in query will time out, default is no timeout
  query_timeout : 10000, // number of milliseconds before a query call will timeout, default is no timeout
  connectionTimeoutMillis : 10000,
};

// DB Pool
const pool = new Pool(config);

pool.on('error', function (err, client) {
    console.error('Database connection error', err.stack);
});

module.exports = pool;