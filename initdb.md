# Local DBMS setups

Local instructions to restore and run the DB Server

## Run DBMS container

docker run -it --name pg_db --rm --network maps_network -e POSTGRES_PASSWORD=mypassword mdillon/postgis

## Connect to DBMS and create the database

psql -h 192.168.0.2 -U postgres
 create database dbparakou

## Restore DB from file

pg_restore -h 192.168.0.2 -U postgres -C -d dbparakou Desktop/parakou/dbparakou

docker run -d --rm --name pg_db --network maps_network -e POSTGRES_PASSWORD=mypassword mdillon/postgis \
	&& sleep 60 \
	&& export PGPASSWORD="mypassword" \
	&& psql -h 192.168.0.2 -U postgres -c 'create database dbparakou;' \
	&& pg_restore -h 192.168.0.2 -U postgres -C -d dbparakou Desktop/parakou/dbparakou

docker run -d --rm --name "geoserver" --network maps_network -v /var/run/docker.sock:/var/run/docker.sock -v ~/kode/nodejs/geoserver/data/data:/geoserver_data/data -p 8080:8080 geonode/geoserver
