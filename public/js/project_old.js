/*******************************/
// Map CSR Plugin initialization
/*******************************/
proj4.defs("urn:ogc:def:crs:EPSG::3857", '+proj=utm +zone=31 +datum=WGS84 +units=m +no_defs');

/*******************************/
// Utils variables and functions
/*******************************/
$('#log').hide();
$('#log button.close').click(() => {
  $('#log').hide();
});
var zoom_qip = $("#mapid").attr('qip') ? $("#mapid").attr('qip') : -1;
var zoom_once = false;

// Map layers color
layersColor = {
  "Parcelles" : {"color": "#999", "tooltipClass": "tooltipParcelles"},
  "Routes" : {"color": "yellow", "tooltipClass": "tooltipRoutes"},
  "Ilots" : {"color": "green", "tooltipClass": "tooltipIlots"},
  "Quartiers" : {"color": "red", "tooltipClass": "tooltipQuartiers"},
  "Arrondissements" : {"color": "purple", "tooltipClass": "tooltipArrondissements"},
  "Infrastructures" : {"color": "magenta", "tooltipClass": "tooltipInfrastructures"},
}

/*******************************/
// Initialize the map
/*******************************/

// Map base layers
var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';
var default_tile = L.tileLayer('',{maxZoom: 25}),
    grayscale   = L.tileLayer(mbUrl, {id: 'mapbox/light-v9', tileSize: 512, zoomOffset: -1, attribution: mbAttr}),
    streets  = L.tileLayer(mbUrl, {id: 'mapbox/streets-v11', tileSize: 512, zoomOffset: -1, attribution: mbAttr});

// Create map
var map = L.map('mapid', {
  preferCanvas: true,
  fullscreenControl: true,
  layers: [default_tile]
}).setView([0, 0], 0);
map.spin(true);

// Layer control settings
var baseMaps = {
  'Empty': default_tile,
  "Grayscale": grayscale,
  "Streets": streets
};
var layerControl = L.control.layers(baseMaps).addTo(map);

/*******************************/
// Adding Print Button
/*******************************/

L.control.browserPrint({
  printModes: ["Portrait", "Landscape", "Auto", "Custom"]
}).addTo(map);

function doImage(err, canvas) {
  if(err) {
    console.error(err);
    return;
  }
  let img = document.createElement('img');
  let dimensions = map.getSize();
  img.width = dimensions.x;
  img.height = dimensions.y;
  img.src = canvas.toDataURL();

  let link = document.createElement('a');
  link.href = img.src;
  link.download = 'Parakou Map.png';
  document.body.appendChild(img);
  link.click();
  document.body.removeChild(link);
}

// Map printing click button
$('#printMap').click(function(){
  leafletImage(map, doImage);
});

/*******************************/
// Adding map control
/*******************************/

var info = L.control({position:'bottomleft'});

info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
    this.update();
    return this._div;
};

// method that we will use to update the control based on feature properties passed
info.update = function (props) {
  let description = "";
  if (props) {
    description = "<b>Parcelle: </b>" + props.feature.label + "<br>" +
    "<b>Nom Quartier: </b>" + props.feature.quartier + "<br>" +
    "<b>Code Quartier: </b>" + props.feature.quartierc + "<br>" +
    "<b>Num Lot: </b>" + props.feature.numlot + "<br>" +
    "<b>Proprietaire: </b>" + props.feature.nomprenom_rec + "<br>" +
    "<b>Proprietaire MAJ: </b>" + props.feature.nom_proprio_maj + "<br>" +
    "<b>Source MAJ: </b>" + props.feature.source_maj + "<br>";
  }

  this._div.innerHTML = '<h4>Mairie de Parakou</h4>' +  (props ?
    description : "Sélectionnez une parcelle pour plus d'info");

};

info.addTo(map);

/*******************************/
// Adding map legend
/*******************************/

var legend = L.control({position: 'bottomright'});

legend.onAdd = function (map) {

    var div = L.DomUtil.create('div', 'info legend');

    // loop through our density intervals and generate a label with a colored square for each interval
    for (const layer in layersColor) {
      if (layersColor.hasOwnProperty(layer)) {
        const max_key = Object.keys(layersColor).length
        div.innerHTML +=
        '<i style="background:' + layersColor[layer].color + '"></i> ' + 
        layer + ((layer!=Object.keys(layersColor)[max_key]) ? '<br>' : '');
      }
    }

    return div;
};

legend.addTo(map);

/*******************************/
// Layers Options Setting
/*******************************/

//
// Parcel style
//
var parcelLayer;

var styleParcel = function (feature) {
    return {
        color : layersColor['Parcelles'].color,
        opacity : 1,
        weight : 1,
        fillColor : "blue",
        fillOpacity : 0.2,
    }
};

// Parcel interactions
// hover and update info
var parcelHighlight = function (e) {
    var layer = e.target;

    layer.setStyle({
        weight: 3,
        color: '#666',
        fillColor : "#FC4E2A",
        fillOpacity: 0.7,
    });

    info.update({"name": "Parcelle", "feature" : layer.feature.properties});

    if (layer.feature.properties && layer.feature.properties.label) {
      let label = layer.feature.properties.label;
      layer.bindTooltip(label, {opacity: 1, className: "mylabel"}).openTooltip();
    }

    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
    }
};
// mouseOut
var parcelResetHighlight = function (e) {
    parcelLayer.resetStyle(e.target);
    info.update();
};
// Click
var zoomToParcel = function (e) {
    map.fitBounds(e.target.getBounds());
};


// Parcel onEachFeature (bind popup, add interaction)
var onEachParcel = function (feature, layer) {
    let properties = feature.properties;
    let description = "";
    if (properties) {
      description = "<div class='row'>" +
      "<div class='col-12 d-flex pb-3'>" +
      "<div class='col-md-4'><img src='/img/logo_parakou.png'></div>" +
      "<div class='col-md-4 font-weight-bold'>Mairie de Parakou</div>" +
      "<div class='col-md-4'><img src='/img/logo_parakou.png'></div>" +
      "</div>" +
      "<div class='col-12'>" +
      "<b>Parcelle: </b>" + properties.label + "<br>" +
      "<b>Nom Quartier: </b>" + properties.quartier + "<br>" +
      "<b>Code Quartier: </b>" + properties.quartierc + "<br>" +
      "<b>Num Lot: </b>" + properties.numlot + "<br>" +
      "<b>Proprietaire: </b>" + properties.nomprenom_rec + "<br>" +
      "<b>Proprietaire MAJ: </b>" + properties.nom_proprio_maj + "<br>" +
      "<b>Source MAJ: </b>" + properties.source_maj + "<br>" +
      "</div>" +
      "</div>";
    }
    //var popupContent = "<p>Parcelle de qip <span class='text-primary'>" + feature.properties.qip + "</span><p/>"
    layer.bindPopup(description);

    layer.on({
        mouseover: parcelHighlight,
        mouseout: parcelResetHighlight,
        click: zoomToParcel
    });


    if (feature.properties.qip == zoom_qip) {
      zoom_once = true;
      map.fitBounds(layer.getBounds());
      layer.setStyle({
        weight: 5,
        color: '#666',
        fillColor : "#FC4E2A",
        fillOpacity: 0.7,
      });
    }
};


//Oneach feature callback for other layers
function onEachFeature(feature, layer) {
    if (feature.properties && feature.properties.label) {
      let label = feature.properties.label;
      layer.bindTooltip( "<span style='color:red;'>" + label.toString() + "</span>", {permanent:true}).openTooltip();
    }
}

function onEachFeatureClosure(layerParams) {
  return function onEachFeature(feature, layer) {
    if (feature.properties && feature.properties.label) {
      let label = feature.properties.label;
      layer.bindTooltip(label, {opacity: 1, permanent:true, className: layerParams.tooltipClass}).openTooltip();
    }
  }  
}

/*******************************/
// AJAX map data fetching function
/*******************************/

function getMapData(url, layerName, map, layerControl, ajax_settings, layerStyle, moreSuccessCallback, moreErrorCallback, completeCallback) {
    let base_url = "";
    $.ajax({
      url: base_url + url,
      method: ajax_settings["method"],
      contentType: ajax_settings["contentType"],
      dataType : ajax_settings["dataType"],
      timeout : ajax_settings["timeout"],
      error : function(jqXHR, textStatus, errorThrown) {
        // Show and log error
        let ajax_error = "Erreur lors du chargement de la couche "+ layerName + ". Veuillez réessayer.";
        $('#log .card-text').append(ajax_error + "<hr>");
        $('#log').show();
        console.error("Une Erreur est survenue lors du chargement de la couche " + layerName + " : " + textStatus);

        // In case of Additional stuff when calling the function
        if (typeof moreErrorCallback == 'function') {
          moreErrorCallback(jqXHR, textStatus, errorThrown);
        }
      },
      success: function(data) {
        // if there is some error
        if (data.error) {
          $('#log .card-text').append(data.error.message + "<hr>");
          $('#log').show();
          return;
        }

        console.info(layerName + ' layer data successfully fetched !');
        let layer = L.Proj.geoJson(data, layerStyle).addTo(map);
    
        //add it to the map control
        layerControl.addOverlay(layer, layerName);

        // In case of Additional stuff when calling the function
        if (typeof moreSuccessCallback == 'function') {
          moreSuccessCallback(data, layer);
        }
      },
      complete: completeCallback,
    });
}

/*******************************/
// Fetch map data in GeoJSON
/*******************************/
var ajax_settings = {
  method: 'GET',
  contentType: 'application/json; charset=utf-8',
  dataType : 'json',
  timeout : 25000,
};

// XHR Parcelles
var parcelles = null;
getMapData("/maps/parcels", "Parcelles", map, layerControl, ajax_settings, {
  style : styleParcel,
  onEachFeature: onEachParcel,
  },
  function (data, layer) {
    parcelles = data;
    parcelLayer = layer;

    // select feature
    if (!zoom_once) {
      if (zoom_qip != -1) {
        $('#log .card-text').text("Le QIP recherché est introuvable");
        $('#log').show();
      }
      map.fitBounds(layer.getBounds());
    }
  },
  null,
  function () {
    map.spin(false);
  });

// XHR Ilots
var ilots;
getMapData("/maps/ilots", "Ilots", map, layerControl, ajax_settings, {
  interactive : false,
  color : layersColor['Ilots'].color,
  weight : 2,
  opacity: 0.6,
  fillColor: 'transparent',
  //onEachFeature : onEachFeatureClosure(layersColor['Ilots']),
  },
  function (data) {
    ilots = data;
});

// XHR Routes
var routes;
getMapData("/maps/routes", "Routes", map, layerControl, ajax_settings, {
  interactive : false,
  color : layersColor['Routes'].color,
  weight : 2,
  },
  function (data) {
    routes = data;
});

// XHR Quartiers
var quartiers;
getMapData("/maps/quartiers", "Quartiers", map, layerControl, ajax_settings, {
  interactive : false,
  color : layersColor['Quartiers'].color,
  weight: 3,
  fillColor : "transparent",
  //onEachFeature : onEachFeatureClosure(layersColor['Quartiers']),
  },
  function (data) {
    quartiers = data;
});

// XHR Arrondissements
var arrondissements;
getMapData("/maps/arrondissements", "Arrondissements", map, layerControl, ajax_settings, {
  interactive : false,
  color : layersColor['Arrondissements'].color,
  weight: 3,
  fillColor : "transparent",
  //onEachFeature : onEachFeatureClosure(layersColor['Arrondissements']),
  },
  function (data) {
    arrondissements = data;
});

// XHR Infrastructures
/*
var infrastructures;
getMapData("/maps/infrastructures", "Infrastructures", map, layerControl, ajax_settings, {
  interactive : false,
  color : layersColor['Infrastructures'].color,
  weight: 3,
  fillColor : "transparent",
  },
  function (data) {
    infrastructures = data;
});
*/
/*******************************/
// Other map setting
/*******************************/
