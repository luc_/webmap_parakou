/*******************************/
// Map CSR Plugin initialization
/*******************************/
proj4.defs("urn:ogc:def:crs:EPSG::3857", '+proj=utm +zone=31 +datum=WGS84 +units=m +no_defs');

/*******************************/
// Utils variables and functions
/*******************************/
$('#log').hide();
$('#log button.close').click(() => {
  $('#log').hide();
});
var zoom_qip = $("#mapid").attr('qip') ? $("#mapid").attr('qip') : -1;
var zoom_once = false;

// Map layers color
layersColor = {
  "Parcelles" : {"color": "#999", "tooltipClass": "tooltipParcelles"},
  "Routes" : {"color": "yellow", "tooltipClass": "tooltipRoutes"},
  "Ilots" : {"color": "green", "tooltipClass": "tooltipIlots"},
  "Quartiers" : {"color": "red", "tooltipClass": "tooltipQuartiers"},
  "Arrondissements" : {"color": "purple", "tooltipClass": "tooltipArrondissements"},
  "Infrastructures" : {"color": "magenta", "tooltipClass": "tooltipInfrastructures"},
}

/*******************************/
// Initialize the map
/*******************************/

// Map base layers
var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';
var default_tile = L.tileLayer('',{maxZoom: 30}),
    grayscale   = L.tileLayer(mbUrl, {id: 'mapbox/light-v9', tileSize: 512, zoomOffset: -1, attribution: mbAttr}),
    streets  = L.tileLayer(mbUrl, {id: 'mapbox/streets-v11', tileSize: 512, zoomOffset: -1, attribution: mbAttr});

// Create map
var map = L.map('mapid', {
  fullscreenControl: true,
  layers: [default_tile]
}).setView([9.3465367, 2.637003], 14);

// Layer control settings
var baseMaps = {
  'Empty': default_tile,
  "Grayscale": grayscale,
  "Streets": streets
};
var layerControl = L.control.layers(baseMaps).addTo(map);

// Parcelle Geojson layer (displayed after a search)
var styleParcel = function (feature) {
  return {
    color : layersColor['Parcelles'].color,
    opacity : 1,
    weight : 1,
    fillColor : "blue",
    fillOpacity : 0.2,
  }
};
var parcelLayer = L.Proj.geoJson(null, styleParcel).addTo(map);;

// Create Geoserver data source
var popup = L.popup();
var geoserver_url = "http://localhost:8080/geoserver/sigpark/wms";

$.ajax({
  url: '/search/url',
  async:false,
  success :(data) => {
  geoserver_url = data;
  }
});

var MySource = L.WMS.Source.extend({
  'getIdentifyLayers': function() {
    return ['sigpark:parcelles'];
  },
  'ajax': function (url, callback) {
    var context = this;
    $.ajax({
      url: url,
      method: 'GET',
      dataType: 'json',
      success : function (data) {
        callback.call(context, data);
      },
      error : function (data) {
        callback.call(context, "error");
      }
    });
  },
  'showFeatureInfo': function(latlng, info) {
    console.log("Showing feature : " + info);
    if (!this._map) {
      return;
    }
    
    let description = "";
    description = "<div class='row'>" +
    "<div class='col-12 d-flex pb-3'>" +
    "<div class='col-md-4'><img src='/img/logo_parakou.png'></div>" +
    "<div class='col-md-4 font-weight-bold'>Mairie de Parakou</div>" +
    "<div class='col-md-4'><img src='/img/logo_parakou.png'></div>" +
    "</div>" +
    "<div class='col-md-12'>" + info + "</div>" +
    "</div>";
    
    //var popupContent = "<p>Parcelle de qip <span class='text-primary'>" + feature.properties.qip + "</span><p/>"
    //console.info(e);
    /*
    let properties = info.features.properties;
    let description = "";
    if (properties) {
      description = "<div class='row'>" +
      "<div class='col-12 d-flex pb-3'>" +
      "<div class='col-md-4'><img src='/img/logo_parakou.png'></div>" +
      "<div class='col-md-4 font-weight-bold'>Mairie de Parakou</div>" +
      "<div class='col-md-4'><img src='/img/logo_parakou.png'></div>" +
      "</div>" +
      "<div class='col-12'>" +
      "<b>Parcelle: </b>" + properties.label + "<br>" +
      "<b>Nom Quartier: </b>" + properties.quartier + "<br>" +
      "<b>Code Quartier: </b>" + properties.quartierc + "<br>" +
      "<b>Num Lot: </b>" + properties.numlot + "<br>" +
      "<b>Proprietaire: </b>" + properties.nomprenom_rec + "<br>" +
      "<b>Proprietaire MAJ: </b>" + properties.nom_proprio_maj + "<br>" +
      "<b>Source MAJ: </b>" + properties.source_maj + "<br>" +
      "</div>" +
      "</div>";
    }
    */

    popup
        .setLatLng(latlng)
        .setContent(description)
        .openOn(this._map);
  }
});
var source = new MySource(geoserver_url, {
    format: 'image/png',
    maxZoom: 30,
    transparent: true,
    version: '1.1.0',
    info_format: "application/json",
    attribution: "Mairie de Parakou"
});
// adding layers
layer_arrondis_wms = source.getLayer("sigpark:arrondissements").addTo(map);
layer_ilots_wms = source.getLayer("sigpark:ilots_reserves").addTo(map);
layer_parcelle_wms = source.getLayer("sigpark:parcelles").addTo(map);

// Add layers to control
layerControl.addOverlay(layer_arrondis_wms, "Arrondissements");
layerControl.addOverlay(layer_ilots_wms, "Ilots");
layerControl.addOverlay(layer_parcelle_wms, "Parcelles");

/*******************************/
// AJAX map data fetching function
/*******************************/

function getMapData(url, layerName, map, ajax_settings, moreSuccessCallback, moreErrorCallback, completeCallback) {
    let base_url = "";
    $.ajax({
      url : base_url + url,
      data : ajax_settings["data"],
      method : ajax_settings["method"],
      contentType : ajax_settings["contentType"],
      dataType : ajax_settings["dataType"],
      timeout : ajax_settings["timeout"],
      beforeSend : function() {
        map.spin(true);
      },
      error : function(jqXHR, textStatus, errorThrown) {
        // Show and log error
        let ajax_error = "Erreur lors du chargement de la couche "+ layerName + ". Veuillez réessayer.";
        $('#log .card-text').append(ajax_error + "<hr>");
        $('#log').show();
        console.error("Une Erreur est survenue lors du chargement de la couche " + layerName + " : " + textStatus);

        // In case of Additional stuff when calling the function
        if (typeof moreErrorCallback == 'function') {
          moreErrorCallback(jqXHR, textStatus, errorThrown);
        }
      },
      success : function(data) {
        // if there is some error
        if (data.error) {
          $('#log .card-text').append(data.error.message + "<hr>");
          $('#log').show();
          return;
        }

        if (data.features && data.features.length == 0) {
          console.error("0 result");
          $('#log .card-text').append("0 parcelles trouvées" + "<hr>");
          $('#log').show();
          return;
        }

        console.info(layerName + ' layer data successfully fetched !');
        parcelLayer.clearLayers();
        parcelLayer.addData(data);

        // In case of Additional stuff when calling the function
        if (typeof moreSuccessCallback == 'function') {
          moreSuccessCallback(data, parcelLayer);
        }
      },
      complete: function() {
        map.spin(false);
        if (typeof completeCallback == 'function') {
          completeCallback();
        }
      },
    });
}

/*******************************/
// Fetch map data in GeoJSON
/*******************************/
var request_data = {
  "commune": "PARAKOU",
  "quartier": null,
  "ilot": null,
  "parcelle": null,
  "qip": null
};

var ajax_settings = {
  method: 'POST',
  data: JSON.stringify(request_data),
  contentType: 'application/json; charset=UTF-8',
  dataType : 'json',
  timeout : 25000,
};

// XHR Parcelles when QIP is given in url when loading page
if (zoom_qip != -1) {
  ajax_settings.data = JSON.stringify({"qip": zoom_qip });
  getMapData("/search", "Recherche", map, ajax_settings,
    function (data, layer) {
      // select feature
      if (!zoom_once) {
        if (zoom_qip == -1) {
          $('#log .card-text').text("Le QIP recherché est introuvable");
          $('#log').show();
        }
        map.fitBounds(layer.getBounds());
        zoom_once = true;
      }
    });
}

// Map Search bar buttons
$('#search-tf').click(() => {
  let qip = $('#input-tf').val();
  ajax_settings.data = JSON.stringify({"qip": qip });
  getMapData("/search", "Recherche", map, ajax_settings,
    function (data, layer) {
      map.fitBounds(layer.getBounds());
    });
});
$('#search').click(() => {
  let quartier = $('#quartier').val();
  let ilot = $('#ilot').val();
  let parcelle = $('#parcelle').val();
  let data = {
    "commune": "PARAKOU",
    "quartier": quartier,
    "ilot": ilot,
    "parcelle": parcelle,
  };
  ajax_settings.data = JSON.stringify(data);
  getMapData("/search", "Recherche", map, ajax_settings,
    function (data, layer) {
      map.fitBounds(layer.getBounds());
    });
});
$('#reset-search-tf, #reset-search').click(() => {
  parcelLayer.clearLayers();
});

/*******************************/
// Adding map control
/*******************************/

var info = L.control({position:'bottomleft'});
info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
    this._div.innerHTML = '<h4>Mairie de Parakou</h4>' + "Sélectionnez une parcelle pour plus d'info";
    //this.update();
    return this._div;
};
info.addTo(map);

/*******************************/
// Adding map legend
/*******************************/

var legend = L.control({position: 'bottomright'});
legend.onAdd = function (map) {
  var div = L.DomUtil.create('div', 'info legend collapse');
  div.id = "map-ctl-legend";
  // loop through our color intervals and generate a label with a colored square for each interval
  for (const layer in layersColor) {
    if (layersColor.hasOwnProperty(layer)) {
      const max_key = Object.keys(layersColor).length
      div.innerHTML +=
      '<i style="background:' + layersColor[layer].color + '"></i> ' + 
      layer + ((layer!=Object.keys(layersColor)[max_key]) ? '<br>' : '');
    }
  }
  return div;
};
legend.addTo(map);
