/*******************************/
// Map CSR Plugin initialization
/*******************************/
proj4.defs("urn:ogc:def:crs:EPSG::3857", '+proj=utm +zone=31 +datum=WGS84 +units=m +no_defs');

/*******************************/
// Utils variables and functions
/*******************************/
$('#log').hide();
$('#log button.close').click(() => {
  $('#log').hide();
});
var zoom_qip = $("#mapid").attr('qip') ? $("#mapid").attr('qip') : -1;
var zoom_once = false;

// Map layers color
layersColor = {
  "Parcelles" : {"color": "#999", "tooltipClass": "tooltipParcelles"},
  "Routes" : {"color": "yellow", "tooltipClass": "tooltipRoutes"},
  "Ilots" : {"color": "green", "tooltipClass": "tooltipIlots"},
  "Quartiers" : {"color": "red", "tooltipClass": "tooltipQuartiers"},
  "Arrondissements" : {"color": "purple", "tooltipClass": "tooltipArrondissements"},
  "Infrastructures" : {"color": "magenta", "tooltipClass": "tooltipInfrastructures"},
}

/*******************************/
// Initialize the map
/*******************************/

// Map base layers
var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';
var default_tile = L.tileLayer('',{maxZoom: 30}),
    grayscale   = L.tileLayer(mbUrl, {id: 'mapbox/light-v9', tileSize: 512, zoomOffset: -1, attribution: mbAttr}),
    streets  = L.tileLayer(mbUrl, {id: 'mapbox/streets-v11', tileSize: 512, zoomOffset: -1, attribution: mbAttr});

// Create map
var map = L.map('mapid', {
  fullscreenControl: true,
  layers: [default_tile]
}).setView([9.3465367, 2.637003], 14);
//452458.75741, 1037616.35389

// Layer control settings
var baseMaps = {
  'Empty': default_tile,
  "Grayscale": grayscale,
  "Streets": streets
};
var layerControl = L.control.layers(baseMaps).addTo(map);

// Parcelle Geojson layer (from search)
var styleParcel = function (feature) {
  return {
    color : layersColor['Parcelles'].color,
    opacity : 1,
    weight : 1,
    fillColor : "blue",
    fillOpacity : 0.2,
  }
};

var parcelLayer = L.Proj.geoJson(null, styleParcel).addTo(map);;

// Geoserver layers
var layer_arrondis_wms = L.tileLayer.wms("http://localhost:8080/geoserver/sigpark/wms", {
    layers: 'sigpark:arrondissements',
    format: 'image/png',
    maxZoom: 30,
    transparent: true,
    version: '1.1.0',
    attribution: "Arrondissement | Mairie de Parakou"
});
layer_arrondis_wms.addTo(map);

var layer_ilots_wms = L.tileLayer.wms("http://localhost:8080/geoserver/sigpark/wms", {
    layers: 'sigpark:ilots_reserves',
    format: 'image/png',
    maxZoom: 30,
    transparent: true,
    version: '1.1.0',
    attribution: "Ilots | Mairie de Parakou"
});
layer_ilots_wms.addTo(map);

var layer_parcelle_wms = L.tileLayer.wms("http://localhost:8080/geoserver/sigpark/wms", {
    layers: ' sigpark:parcelles',
    format: 'image/png',
    maxZoom: 30,
    transparent: true,
    version: '1.1.0',
    attribution: "Parcelles | Mairie de Parakou"
});
layer_parcelle_wms.addTo(map);

// add it to the map control
layerControl.addOverlay(layer_arrondis_wms, "Arrondissements");
layerControl.addOverlay(layer_ilots_wms, "Ilots");
layerControl.addOverlay(layer_parcelle_wms, "Parcelles");

// Map interactions
var popup = L.popup();

var onEachParcel_2 = function (e) {
    let description = "";
    description = "<div class='row'>" +
    "<div class='col-12 d-flex pb-3'>" +
    "<div class='col-md-4'><img src='/img/logo_parakou.png'></div>" +
    "<div class='col-md-4 font-weight-bold'>Mairie de Parakou</div>" +
    "<div class='col-md-4'><img src='/img/logo_parakou.png'></div>" +
    "</div>" +
    "</div>";
    //var popupContent = "<p>Parcelle de qip <span class='text-primary'>" + feature.properties.qip + "</span><p/>"
    console.info(e);
    popup
        .setLatLng(e.latlng)
        .setContent(description)
        .openOn(map);
};


function onMapClick(e) {
    alert("You clicked the map at " + e.latlng);
}
//map.on('click', onEachParcel_2);

/*******************************/
// Layers Options Setting
/*******************************/

//
// Parcelle layer style
//

/*******************************/
// AJAX map data fetching function
/*******************************/

function getMapDataInfo() {
  let base_url = "http://localhost:8080/geoserver/sigpark/wms";
  var data = {
    SERVICE =	"WMS",
    VERSION	= "1.1.1",
    REQUEST	= "GetFeatureInfo",
    FORMAT = "image/png",
    TRANSPARENT =	true,
    QUERY_LAYERS = "sigpark:arrondissements",
    LAYERS = "sigpark:arrondissements",
    exceptions = "application/vnd.ogc.se_inimage",
    INFO_FORMAT = "application/json",
    FEATURE_COUNT = 50,
    X = 50,
    Y = 50,
    SRS = "EPSG:32631",
    WIDTH = 101,
    HEIGHT = 101,
    BBOX = ""
  }
  $.ajax({
    url : base_url,
    data : ajax_settings["data"],
    method : ajax_settings["method"],
    contentType : ajax_settings["contentType"],
    dataType : ajax_settings["dataType"],
    timeout : ajax_settings["timeout"],
  });
}

function getMapData(url, layerName, map, ajax_settings, moreSuccessCallback, moreErrorCallback, completeCallback) {
    let base_url = "";
    $.ajax({
      url : base_url + url,
      data : ajax_settings["data"],
      method : ajax_settings["method"],
      contentType : ajax_settings["contentType"],
      dataType : ajax_settings["dataType"],
      timeout : ajax_settings["timeout"],
      beforeSend : function() {
        map.spin(true);
      },
      error : function(jqXHR, textStatus, errorThrown) {
        // Show and log error
        let ajax_error = "Erreur lors du chargement de la couche "+ layerName + ". Veuillez réessayer.";
        $('#log .card-text').append(ajax_error + "<hr>");
        $('#log').show();
        console.error("Une Erreur est survenue lors du chargement de la couche " + layerName + " : " + textStatus);

        // In case of Additional stuff when calling the function
        if (typeof moreErrorCallback == 'function') {
          moreErrorCallback(jqXHR, textStatus, errorThrown);
        }
      },
      success : function(data) {
        // if there is some error
        if (data.error) {
          $('#log .card-text').append(data.error.message + "<hr>");
          $('#log').show();
          return;
        }

        if (data.features && data.features.length == 0) {
          console.error("0 result");
          $('#log .card-text').append("0 parcelles trouvées" + "<hr>");
          $('#log').show();
          return;
        }

        console.info(layerName + ' layer data successfully fetched !');
        parcelLayer.clearLayers();
        parcelLayer.addData(data);

        // In case of Additional stuff when calling the function
        if (typeof moreSuccessCallback == 'function') {
          moreSuccessCallback(data, parcelLayer);
        }
      },
      complete: function() {
        map.spin(false);
        if (typeof completeCallback == 'function') {
          completeCallback();
        }
      },
    });
}

/*******************************/
// Fetch map data in GeoJSON
/*******************************/
var request_data = {
  "commune": "PARAKOU",
  "quartier": null,
  "ilot": null,
  "parcelle": null,
  "qip": null
};

var ajax_settings = {
  method: 'POST',
  data: JSON.stringify(request_data),
  contentType: 'application/json; charset=UTF-8',
  dataType : 'json',
  timeout : 25000,
};

// XHR Parcelles
if (zoom_qip != -1) {
  ajax_settings.data = JSON.stringify({"qip": zoom_qip });
  getMapData("/search", "Recherche", map, ajax_settings,
    function (data, layer) {
      parcelLayer = layer;
  
      // select feature
      if (!zoom_once) {
        if (zoom_qip == -1) {
          $('#log .card-text').text("Le QIP recherché est introuvable");
          $('#log').show();
        }
        map.fitBounds(layer.getBounds());
      }
    });
}

// Handle searchbar
/*
$('#ctl-search').click(() => {
  console.log("OKKK");
  if (!$('#map-ctl-search').hasClass('collapse show') ) {
    $('#map-ctl-search').addClass('h-100');
    $('#map-ctl-search').collapse('toggle');
    console.log("In");
  }
  else {
    $('#map-ctl-search').removeClass('h-100');
    $('#map-ctl-search').collapse('toggle');
  }
});
*/
$('#search-tf').click(() => {
  let qip = $('#input-tf').val();
  ajax_settings.data = JSON.stringify({"qip": qip });
  getMapData("/search", "Recherche", map, ajax_settings,
    function (data, layer) {
      parcelLayer = layer;
      map.fitBounds(parcelLayer.getBounds());
    });
});
$('#search').click(() => {
  let quartier = $('#quartier').val();
  let ilot = $('#ilot').val();
  let parcelle = $('#parcelle').val();
  let data = {
    "commune": "PARAKOU",
    "quartier": quartier,
    "ilot": ilot,
    "parcelle": parcelle,
  };
  ajax_settings.data = JSON.stringify(data);
  getMapData("/search", "Recherche", map, ajax_settings,
    function (data, layer) {
      parcelLayer = layer;
      map.fitBounds(layer.getBounds());
    });
});
// Reset
$('#reset-search-tf, #reset-search').click(() => {
  parcelLayer.clearLayers();
  //parcelLayer.removeFrom(map);
  //map.removeLayer(parcelLayer);
});

/*******************************/
// Other map setting
/*******************************/

/*******************************/
// Adding map control
/*******************************/

var info = L.control({position:'bottomleft'});

info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
    this.update();
    return this._div;
};

// method that we will use to update the control based on feature properties passed
info.update = function (props) {
  let description = "";
  if (props) {
    description = "<b>Parcelle: </b>" + props.feature.label + "<br>" +
    "<b>Nom Quartier: </b>" + props.feature.quartier + "<br>" +
    "<b>Code Quartier: </b>" + props.feature.quartierc + "<br>" +
    "<b>Num Lot: </b>" + props.feature.numlot + "<br>" +
    "<b>Proprietaire: </b>" + props.feature.nomprenom_rec + "<br>" +
    "<b>Proprietaire MAJ: </b>" + props.feature.nom_proprio_maj + "<br>" +
    "<b>Source MAJ: </b>" + props.feature.source_maj + "<br>";
  }

  this._div.innerHTML = '<h4>Mairie de Parakou</h4>' +  (props ?
    description : "Sélectionnez une parcelle pour plus d'info");

};

info.addTo(map);

/*******************************/
// Adding map legend
/*******************************/

var legend = L.control({position: 'bottomright'});

legend.onAdd = function (map) {

    var div = L.DomUtil.create('div', 'info legend collapse');
    div.id = "map-ctl-legend";

    // loop through our color intervals and generate a label with a colored square for each interval
    for (const layer in layersColor) {
      if (layersColor.hasOwnProperty(layer)) {
        const max_key = Object.keys(layersColor).length
        div.innerHTML +=
        '<i style="background:' + layersColor[layer].color + '"></i> ' + 
        layer + ((layer!=Object.keys(layersColor)[max_key]) ? '<br>' : '');
      }
    }

    return div;
};

legend.addTo(map);
