# Modifications

1. Pouvoir cocher et decocher les couches (** Current **)

2. Add lettre parcelles, numero lots, nom quartier (sur la carte, dans les zones correspondantes)
3. Info complémentaire sur chaque clique de parcelle (Quartiernom, Quatiercode, numLot, LettreP, proprietaire:nom\_recasé, NPprop\_MAJ:NompropriomMAJ, SourceMAJ)
4. Imprimer si possible, et screenshot, (voir si on peut choisir les champs à afficher pour la capture) 
5. Penser à afficher les informations d'autres trucs genre ilots, quartiers.
6. Modifier les clefs d'API Tilelayer avant deployment

## Other modifications

1. Couleurs couches
2. Enlever Tiles layers
3. Ajouts de nouvelles couches : arrondissements, infrastructures
4. Certaines couches doivent être non decochables

5. Ajouter nom Quartier
6. Ajouter infobulle pour printing
7. Ajouter ilost reserve (mauve au lieu de vert)

## Issues

1. Tooltip data not rendering in map export image
2. layer.bringToFront()

## PLugins SearchLayers

1. Elargir et Customizer la barre des plugins et des outils dans QGIS
2. Penser à tout selecitionner apres les recherches dans le map QGIS et la table attributaire QGIS
3. Selectionner parcelles apres recherches sur mutations, rfu, recasements, bornages, adc, enrollements,  
4. Mutations find and select parcelles

## PLugins cadastre

## Instructions

Installer "npm" et "nodejs"

Taper npm install

Configurer les parametres de la BD dans "database/credentials.js"

Configurer le host et le port dans le fichier server.js (localhost:5000 par defaut)

Taper "node server.js" pour lancer le serveur Web

## Frontend

not use IP address frontend api call
display: none; and not hidden
'method' for xhr and not 'type'
(bootstrap 4.4)
