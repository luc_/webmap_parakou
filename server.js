/*
########################################
                Packages
########################################
*/
var express = require("express");
var cors = require('cors');
var morgan = require('morgan');
/*
########################################
                Main variables
########################################
*/
var app = express();
var host = "localhost";
var port = 5000;

app.set('view engine', 'ejs');

/*
########################################
                Middlewares
########################################
*/
app.use(express.static('public'));
app.use(morgan('common'));
app.use(express.json());

// Allow CORS
app.use(cors());

/*
########################################
                Routes
########################################
*/

var maps_routes = require('./routes/maps.js');
app.use('/maps', maps_routes);

var search_routes = require('./routes/search.js');
app.use('/search', search_routes);

var index_routes = require('./routes/index.js');
app.use('/', index_routes);

app.use(function (req, res, next) {
  res.status(404).render("404");
});


/*
########################################
                Launch app
########################################
*/
app.listen(port, host, function() {
    console.log("Server started on " + host + ":" + port);
});

module.exports = app;
