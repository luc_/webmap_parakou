var express = require('express');
var router = express.Router();

/*
########################################
                Routes
########################################
*/
router.get('/', function(req, res){
	res.redirect('/maps');
});

// Export this router to use in our main module
module.exports = router;
