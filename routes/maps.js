var express = require('express');
var mapdata = require('../database/mapdata.js');

var router = express.Router();

/*
########################################
                Routes
########################################
*/

// API Routes
router.get('/parcels', function(req, res){
	mapdata.getGeoJsonParcels(function(err, result) {
		if (err) {
			res.json({error : err});
		}
		else {
			res.json(result);
		}
	});
});

router.get('/routes', function(req, res){
	mapdata.getGeoJsonRoutes(function(err, result) {
		if (err) {
			res.json({error : err});
		}
		else {
			res.json(result);
		}
	});
});

router.get('/ilots', function(req, res){
	mapdata.getGeoJsonIlots(function(err, result) {
		if (err) {
			res.json({error : err});
		}
		else {
			res.json(result);
		}
	});
});

router.get('/quartiers', function(req, res){
	mapdata.getGeoJsonQuartiers(function(err, result) {
		if (err) {
			res.json({error : err});
		}
		else {
			res.json(result);
		}
	});
});

router.get('/arrondissements', function(req, res){
	mapdata.getGeoJsonArrondissements(function(err, result) {
		if (err) {
			res.json({error : err});
		}
		else {
			res.json(result);
		}
	});
});

router.get('/infrastructures', function(req, res){
	mapdata.getGeoJsonInfrastructures(function(err, result) {
		if (err) {
			res.json({error : err});
		}
		else {
			res.json(result);
		}
	});
});

// Other Routes
router.get('/', function(req, res){
	res.render('map', {"qip" : -1});
});

router.get('/:qip', function(req, res){
	var qip = req.params.qip;
	res.render('map', {"qip" : qip});
});

// Export this router to use in our main module
module.exports = router;