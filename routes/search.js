var express = require('express');
var mapdata = require('../database/mapdata.js');
var apierrors = require('../models/apierrors')

var router = express.Router();

/*
########################################
                Routes
########################################
*/
// Geoserver Url
router.get('/url', function(req, res){
    var geo_url = "http://localhost:8080/geoserver/sigpark/wms";
    return res.send(geo_url);
});

// API Routes
router.post('/', function(req, res){
    data = req.body;
    let text = "";
    let values = "";
    if (data.qip) {
        text = 'SELECT ST_AsGeoJSON(geom) AS geometry from parcelles WHERE qip = $1';
        values = [data.qip];
    }
    else if (data.quartier && (!data.ilot) && (!data.parcelle)) {
        text = 'SELECT ST_AsGeoJSON(geom) AS geometry from parcelles WHERE quartier = $1';
        values = [data.quartier];
    }
    else if (data.quartier && data.ilot && (!data.parcelle)) {
        text = 'SELECT ST_AsGeoJSON(geom) AS geometry from parcelles WHERE quartier = $1 AND numlot = $2';
        values = [data.quartier, data.ilot];
    }
    else if (data.quartier && (!data.ilot) && data.parcelle) {
        text = 'SELECT ST_AsGeoJSON(geom) AS geometry from parcelles WHERE quartier = $1 AND parcelle = $2';
        values = [data.quartier, data.parcelle];
    }
    else if (data.quartier && data.ilot && data.parcelle) {
        text = 'SELECT ST_AsGeoJSON(geom) AS geometry from parcelles WHERE quartier = $1 AND numlot = $2 AND  parcelle = $3';
        values = [data.quartier, data.ilot, data.parcelle];
    }
    else {
        return res.json({"error" : apierrors['error_4']});
    }

    // Make the query
    mapdata.getGeoJsonFromQuery(text, values, (err, result) => {
        if (err) {
			res.json({error : err});
		}
		else {
			res.json(result);
		}
    });
});

// Export this router to use in our main module
module.exports = router;