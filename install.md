# Procédure d'installation

1. Installer Nodejs
2. Installer et configurer geoserver
3. Démmarrer l'application

## Installation nodejs

Lancer le programme dinstallation et suivre les instructions.

## Installation et configuration geoserver

1. Lancer le programme d'installation et suivre les instructions.
2. Creer un workspace nommé sigpark
3. Ajouter la store et la lier à la base de données
4. Ajouter les couches (parcelles, ilots_reserves, arondissements, etc...)
5. Tester dans layer preview

## Démmarrer l'application

L'application est automatiquement lancé par le script d'Elie
